import React from "react";
import styles from "./Sheet.module.css";
import {
  GTCharacter,
  GTInventory,
  GTSvgBow,
  GTSvgControllerNes,
  GTSvgGameboy,
  GTSvgLaptop,
  GTSvgMap,
  GTSvgShield,
  GTSvgSword,
  GTInventorySlot,
  GTSvgSwordLarge,
  GTHearts,
  GTSvgGauge,
} from "@horsty/game-tools";
const size = 164;
const availableItems = {
  helmet: () => (
    <GTInventorySlot item={() => GTSvgMap({ width: size })} name={"Map"} />
  ),
  legs: () => (
    <GTInventorySlot
      item={() => GTSvgSwordLarge({ width: size })}
      name={"Large Sword"}
    />
  ),
  belt: () => (
    <GTInventorySlot item={() => GTSvgBow({ width: size })} name={"Bow"} />
  ),
  accessory: () => (
    <GTInventorySlot
      item={() => GTSvgControllerNes({ width: size })}
      name={"Controller nes"}
    />
  ),
  principalWeapon: () => (
    <GTInventorySlot item={() => GTSvgSword({ width: size })} name={"Sword"} />
  ),
  tool: () => (
    <GTInventorySlot
      item={() => GTSvgGameboy({ width: 96 })}
      name={"Gameboy"}
    />
  ),
  goodies: () => (
    <GTInventorySlot
      item={() => GTSvgShield({ width: size })}
      name={"Shield"}
    />
  ),
  secondaryWeapon: () => (
    <GTInventorySlot
      item={() => GTSvgLaptop({ width: size })}
      name={"Laptop"}
    />
  ),
};

const stats = {
  hearts: 3,
  maxHearts: 5,
};

const HealthPoints = {
  actual: 100,
  max: 100,
};
const MagicPoints = {
  actual: 85,
  max: 100,
};
const Focus = {
  actual: 25,
  max: 100,
};

const Sheet = () => {
  return (
    <div className={`${styles.sheet}`}>
      <section className={`${styles.avatar} nes-container is-rounded`}>
        <GTCharacter move={true} />
      </section>
      <div className={`${styles.character}`}>
        <section className={`${styles.inventory} nes-container is-rounded`}>
          <GTInventory {...availableItems} />
        </section>
        <section className={`${styles.stats} nes-container is-rounded`}>
          <GTHearts {...stats} />
          <label>Health Points :</label>
          <GTSvgGauge {...HealthPoints} />
          <label>Magic Points :</label>
          <GTSvgGauge {...MagicPoints} />
          <label>Focus :</label>
          <GTSvgGauge {...Focus} />
        </section>
      </div>
    </div>
  );
};

export default Sheet;
